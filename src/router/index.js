import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/index',
    name: 'Index',
    component: () => import(/* webpackChunkName: "about" */ '../views/Index.vue')
  },
  {
    path: '/caselist',
    name: 'CaseList',
    component: () => import(/* webpackChunkName: "about" */ '../views/CaseList.vue')
  },
  {
    path: '/joblist',
    name: 'JobList',
    component: () => import(/* webpackChunkName: "about" */ '../views/JobList.vue')
  },
  {
    path: '/tools',
    name: 'Tools',
    component: () => import(/* webpackChunkName: "about" */ '../views/Tools.vue')
  },
  {
    path: '/',
    redirect: '/index',
  }
]

const router = new VueRouter({
  routes
})

export default router
